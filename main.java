public class main {
    public static void main(String[] args) {
    int number = factorialWithoutRecursive(6);
        System.out.println(number);
    }
    public static int factorialWithoutRecursive (int number) {
        int result = 1;

        // факториал БЕЗ РЕКУРСИИ 5 = !5 = 1*2*3*4*5 = 120
        for (int i = 1; i <= number; i++){
            result = result * i;
        }
        return result;
    }
    // факториал с рекурсией
    public static int factorialWithRecursive(int number){
        return number * factorialWithoutRecursive(-1);
    }

}
